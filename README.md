# Aperture Video Screen

Simple video screen with aperture effect shader. 

# Preview

![previewA.png](https://gitlab.com/s-ilent/aperture-video-screen/-/wikis/uploads/3fec4fa3407231fe77715aaf05bf254d/previewA.png.jpg)

![previewB.png](https://gitlab.com/s-ilent/aperture-video-screen/-/wikis/uploads/14122c97bddca366b04ab2e5deeecb64/previewB.png.jpg)

![previewC.png](https://gitlab.com/s-ilent/aperture-video-screen/-/wikis/uploads/3868b2dad97d121fb92a113755277aaf/previewC.png.jpg)

# Usage

Apply it to a material like this.

![inspector](https://gitlab.com/s-ilent/aperture-video-screen/-/wikis/uploads/36a3efd49f51bdb67b770d4a6db13975/inspector.png)