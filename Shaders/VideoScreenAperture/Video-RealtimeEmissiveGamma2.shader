﻿// Upgrade NOTE: upgraded instancing buffer 'Props' to new syntax.
// Realtime Emissive Gamma - TV screen edition
// Additions by Silent
// This uses some fancy techniques to create an aperture grille effect
// that holds up well in VR. Some of this is a bit excessive.

Shader "Video/RealtimeEmissiveGamma TV Screen" {
  Properties {
    [Toggle(FINALPASS)]_HighQualityMode("High quality mode", Float) = 0
    _EmissionMap ("Emissive (RGB)", 2D) = "black" {}
    _Aperture ("Aperture", 2D) = "white" {}
    _Emission ("Emission Scale", Float) = 1
    _EmissionMeta ("Realtime GI Emission Scale", Float) = 1
    _TargetResX ("Min. Horizontal Screen Resolution", Int) = 1920
    _TargetResY ("Min. Vertical Screen Resolution", Int) = 1080
    _Smoothness ("Smoothness", Range(0, 1)) = 0
    [ToggleUI] _IsAVProInput("Is AVPro Input", Float) = 0 
    [Toggle(_INVERSETONEMAP)] _UseInverseTonemap("Reverse tonemapping before display", Float) = 0 
    [ToggleOff(_SPECULARHIGHLIGHTS_OFF)]_SpecularHighlights ("Specular Highlights", Float) = 1.0
    [ToggleOff(_GLOSSYREFLECTIONS_OFF)]_GlossyReflections ("Glossy Reflections", Float) = 1.0
    [HideInInspector]__dummy("dummy", 2D) = "black" {}
  }
  SubShader {
    Tags { "RenderType"="Opaque" }
    LOD 200

    CGPROGRAM
    // Physically based Standard lighting model, and enable shadows on all light types
    #pragma surface surf Standard fullforwardshadows vertex:vert

    // Use shader model 3.0 target, to get nicer looking lighting
    #pragma target 3.0
    #pragma shader_feature FINALPASS
    #pragma shader_feature_local _INVERSETONEMAP
    #pragma shader_feature_local _SPECULARHIGHLIGHTS_OFF
    #pragma shader_feature_local _GLOSSYREFLECTIONS_OFF

    CBUFFER_START(UnityPerMaterial)
    fixed _Emission;
    fixed _EmissionMeta;
    fixed _IsAVProInput;
    fixed _Smoothness;
    sampler2D _EmissionMap;
    sampler2D _Aperture;
    float4 _EmissionMap_TexelSize;
    float4 _Aperture_TexelSize;
    int _TargetResX;
    int _TargetResY;
    CBUFFER_END

    struct Input {
      float2 uv__dummy;
    };

    // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
    // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
    // #pragma instancing_options assumeuniformscaling
    UNITY_INSTANCING_BUFFER_START(Props)
    // put more per-instance properties here

    UNITY_INSTANCING_BUFFER_END(Props)

      void vert (inout appdata_full v) {
          #if defined(LIGHTMAP_SHADOW_MIXING)
          //if(v.vertex.w < 0) {v.texcoord.x += tex2Dlod(unity_lightmap, v.texcoord.xy).w;}
          #endif
      }

float max3(float x, float y, float z) { return max(x, max(y, z)); }
float3 TonemapInvert(float3 c) { return c * rcp(1.0 - max3(c.r, c.g, c.b)); }


float3 InverseNeutralTonemap(float3 x, float A, float B, float C, float D, float E, float F)
{
 // See: http://theagentd.blogspot.com/2013/01/hdr-inverse-tone-mapping-msaa-resolve.html
 return ((sqrt((4*x-4*x*x)*A*D*F*F*F+((4*x-4)*A*D*E+B*B*C*C+(2*x-2)*B*B*C+(x*x-2*x+1)*B*B)*F*F+((2-2*x)*B*B-2*B*B*C)*E*F+B*B*E*E)+((1-x)*B-B*C)*F+B*E)/(2*x*A*F-2*A*E));
}

float3 NeutralTonemapInvert(float3 x)
{
    // Tonemap
    float a = 0.2;
    float b = 0.29;
    float c = 0.24;
    float d = 0.272;
    float e = 0.02;
    float f = 0.3;
    float whiteLevel = 5.3;
    float whiteClip = 1.0;

    float3 whiteScale = InverseNeutralTonemap(1-(1/whiteLevel), a, b, c, d, e, f);
    x = InverseNeutralTonemap(1-(x*whiteScale), a, b, c, d, e, f);
    return x * whiteLevel;
}

// Returns pixel sharpened to nearest pixel boundary. 
// texelSize is Unity _Texture_TexelSize; zw is w/h, xy is 1/wh
float2 sharpSample( float4 texSize , float2 coord )
{
  float2 boxSize = clamp(fwidth(coord) * texSize.zw, 1e-5, 1);
  coord = coord * texSize.zw - 0.5 * boxSize;
  float2 txOffset = smoothstep(1 - boxSize, 1, frac(coord));
  return(floor(coord) + 0.5 + txOffset) * texSize.xy; 
}

float lerpstep( float a, float b, float t)
{
    return saturate( ( t - a ) / ( b - a ) );
}


float simpleSharpen (float x, float width, float mid)
{
    width = max(width, fwidth(x));
    x = lerpstep(mid-width, mid, x);

    return x;
}

fixed4 tex2Dmultisample(sampler2D tex, float2 uv)
{
  float2 dx = ddx(uv) * 0.25;
  float2 dy = ddy(uv) * 0.25;

  float4 sample0 = tex2D(tex, uv + dx + dy);
  float4 sample1 = tex2D(tex, uv + dx - dy);
  float4 sample2 = tex2D(tex, uv - dx + dy);
  float4 sample3 = tex2D(tex, uv - dx - dy);
    
  return (sample0 + sample1 + sample2 + sample3) * 0.25;
}

fixed4 tex2DmultisampleSharp(sampler2D tex, float4 ts, float2 uv)
{
  float2 dx = ddx(uv) * 0.25;
  float2 dy = ddy(uv) * 0.25;

  float4 sample0 = tex2D(tex, sharpSample(ts, uv + dx + dy));
  float4 sample1 = tex2D(tex, sharpSample(ts, uv + dx - dy));
  float4 sample2 = tex2D(tex, sharpSample(ts, uv - dx + dy));
  float4 sample3 = tex2D(tex, sharpSample(ts, uv - dx - dy));
    
  return (sample0 + sample1 + sample2 + sample3) * 0.25;
}

      void surf (Input IN, inout SurfaceOutputStandard o) {

        // Emissive comes from texture
        float2 uv = IN.uv__dummy;
        uv.y = lerp(uv.y, 1-uv.y, _IsAVProInput);


        #if defined(FINALPASS)
        fixed4 e = tex2DmultisampleSharp (_EmissionMap, _EmissionMap_TexelSize, uv);
        #else
        float2 sharpUV = sharpSample(_EmissionMap_TexelSize, uv);
        fixed4 e = tex2D (_EmissionMap, sharpUV);
        #endif

        #if defined(_INVERSETONEMAP)
        e.rgb = NeutralTonemapInvert(e.rgb);
        #endif

        o.Albedo = fixed4(0,0,0,0);
        o.Alpha = e.a;

        // Imagine a perfect monitor
        float2 resScale = 1+floor(float2(_TargetResX, _TargetResY)/_EmissionMap_TexelSize.zw);
        float2 apSharpUV = sharpSample(_Aperture_TexelSize, uv*_EmissionMap_TexelSize.zw*resScale);
        float3 mask = tex2D (_Aperture, apSharpUV);
        //mask = smoothstep(0.5, 0.5+pxWidth, mask);
      
        if (_IsAVProInput == 1)
        {
                e.rgb = pow(e.rgb,2.2);
        }
        #if !UNITY_PASS_META
        o.Emission = e * _Emission * mask;
        #else
        o.Emission = e * _Emission * _EmissionMeta;
        #endif
        o.Metallic = 0;
        o.Smoothness = _Smoothness;
      }
    ENDCG
  }
  FallBack "Video/RealtimeEmissiveGamma"
}
