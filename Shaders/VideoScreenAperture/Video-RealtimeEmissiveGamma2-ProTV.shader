﻿// Upgrade NOTE: upgraded instancing buffer 'Props' to new syntax.
// Realtime Emissive Gamma - TV screen edition
// Additions by Silent
// This uses some fancy techniques to create an aperture grille effect
// that holds up well in VR. Some of this is a bit excessive.

Shader "Silent/ProTV VideoScreen with Aperture" {
  Properties {
    _MainTex("Standby Texture", 2D) = "black" {}
    _SoundTex("Sound-Only Texture", 2D) = "grey" {}
    _VideoTex("Video Texture (Render Texture from the TV goes here)", 2D) = "black" {}
    _Aspect("Target Aspect Ratio (0 to ignore)", Float) = 1.77777
    [Gamma] _Brightness("Base Brightness", Float) = 1
    [Space]
    [Header(Render Options)]
    [Toggle(FINALPASS)]_HighQualityMode("High quality mode", Float) = 0
    [NoScaleOffset]_Aperture ("Aperture", 2D) = "white" {}
    _TargetResX ("Min. Horizontal Screen Resolution", Int) = 640
    _TargetResY ("Min. Vertical Screen Resolution", Int) = 480
    [Space]
    _Emission ("Emission Scale", Float) = 1
    _EmissionMeta ("Realtime GI Emission Scale", Float) = 1
    [Space]
    _Smoothness ("Smoothness", Range(0, 1)) = 0
    [Toggle(_INVERSETONEMAP)] _UseInverseTonemap("Apply inversed tonemap", Float) = 0 
    [Space]
    [Header(ProTV Options)]
    [Enum(Disabled, 0, Standard, 1, Dynamic, 2)] _Mirror("Mirror Flip Mode", Float) = 1
    [Enum(None, 0, Side by Side, 1, Side By Side Swapped, 2, Over Under, 3, Over Under Swapped, 4)] _3D("Standby 3D Mode", Float) = 0
    [Enum(Half Size 3D, 2, Full Size 3D, 0)] _Wide("Standby 3D Mode Size", Float) = 2
    _Spread("Standby 3D Stereo Offset", Float) = 0
    [ToggleUI] _Force2D("Force Standby to 2D", Float) = 0
    [ToggleUI] _Clip("Clip Aspect", Float) = 0
    [Space]
    [Header(System Options)]
    [Enum(UnityEngine.Rendering.CullMode)]_CullMode("Cull Mode", Int) = 0
	[ToggleOff(_SPECULARHIGHLIGHTS_OFF)]_SpecularHighlights ("Specular Highlights", Float) = 1.0
	[ToggleOff(_GLOSSYREFLECTIONS_OFF)]_GlossyReflections ("Glossy Reflections", Float) = 1.0
    [HideInInspector]_dummy("dummy", 2D) = "black" {}
  }
  SubShader {
    Tags { "RenderType"="Opaque" }
    Cull [_CullMode]
    LOD 200

    CGPROGRAM
    // Physically based Standard lighting model, and enable shadows on all light types
    #pragma surface surf Standard fullforwardshadows vertex:vert

    // Use shader model 3.0 target, to get nicer looking lighting
    #pragma target 3.0
    #pragma shader_feature FINALPASS
    #pragma shader_feature_local _INVERSETONEMAP
    #pragma shader_feature_local _SPECULARHIGHLIGHTS_OFF
    #pragma shader_feature_local _GLOSSYREFLECTIONS_OFF

    CBUFFER_START(UnityPerMaterial)
    fixed _Emission;
    fixed _EmissionMeta;
    fixed _Smoothness;

    sampler2D _EmissionMap;
    sampler2D _Aperture;

    float4 _EmissionMap_TexelSize;
    float4 _Aperture_TexelSize;

    int _TargetResX;
    int _TargetResY;
    
    sampler2D _MainTex;
    float4 _MainTex_ST;
    float4 _MainTex_TexelSize;

    sampler2D _SoundTex;
    float4 _SoundTex_ST;
    float4 _SoundTex_TexelSize;

    #ifndef SHADER_TARGET_SURFACE_ANALYSIS
    // Use explicit sampler state to deal with the texture resizing
    Texture2D _VideoTex;
    SamplerState sampler_VideoTex;
    #endif

    #ifdef SHADER_TARGET_SURFACE_ANALYSIS
    #define const
    #endif

    float4 _VideoTex_ST;
    float4x4 _VideoData;

    float _Brightness;
    float _Mirror;
    float _Clip;

    float _3D;
    float _Wide;
    float _Spread;
    float _Force2D;
    float _Aspect;

    uniform float _VRChatMirrorMode;
    uniform float3 _VRChatMirrorCameraPos;
    CBUFFER_END

    struct Input {
      float2 uv_dummy;
    };

    void vert (inout appdata_full v) {
        #if defined(LIGHTMAP_SHADOW_MIXING)
        //if(v.vertex.w < 0) {v.texcoord.x += tex2Dlod(unity_lightmap, v.texcoord.xy).w;}
        #endif
    }

    float max3(float x, float y, float z) { return max(x, max(y, z)); }
    float3 TonemapInvert(float3 c) { return c * rcp(1.0 - max3(c.r, c.g, c.b)); }

    float3 InverseNeutralTonemap(float3 x, float A, float B, float C, float D, float E, float F)
    {
    // See: http://theagentd.blogspot.com/2013/01/hdr-inverse-tone-mapping-msaa-resolve.html
    return ((sqrt((4*x-4*x*x)*A*D*F*F*F+((4*x-4)*A*D*E+B*B*C*C+(2*x-2)*B*B*C+(x*x-2*x+1)*B*B)*F*F
                +((2-2*x)*B*B-2*B*B*C)*E*F+B*B*E*E)+((1-x)*B-B*C)*F+B*E)/(2*x*A*F-2*A*E));
    }

    float3 NeutralTonemapInvert(float3 x)
    {
        // Tonemap
        float a = 0.2;
        float b = 0.29;
        float c = 0.24;
        float d = 0.272;
        float e = 0.02;
        float f = 0.3;
        float whiteLevel = 5.3;
        float whiteClip = 1.0;

        float3 whiteScale = InverseNeutralTonemap(1-(1/whiteLevel), a, b, c, d, e, f);
        x = InverseNeutralTonemap(1-(x*whiteScale), a, b, c, d, e, f);
        return x * whiteLevel;
    }

    float2 aspect_ratio(float2 uv, const float expected_aspect, float2 res, const float2 center)
    {
        if (expected_aspect == 0) return uv; // apsect of 0 means no adjustments made
        if (abs(res.x / res.y - expected_aspect) > .001)
        {
            float2 norm_res = float2(res.x / expected_aspect, res.y);
            const float2 correction = lerp(
                // width needs corrected
                float2(norm_res.x / norm_res.y, 1),
                // height needs corrected
                float2(1, norm_res.y / norm_res.x),
                // determine corrective axis
                norm_res.x > norm_res.y
            );
            // apply normalized correction anchored to given center
            uv = ((uv - center) / correction) + center;
        }
        return uv;
    }

    bool is_mirror() { return _Mirror && _VRChatMirrorMode; }

    bool is_right_eye()
    {
        #ifdef USING_STEREO_MATRICES
        return unity_StereoEyeIndex == 1;
        #else
        // include stereoeyeindex here cause pico is a dumb pos
        return unity_StereoEyeIndex == 1
            || _VRChatMirrorMode == 1 && mul(unity_WorldToCamera, float4(_VRChatMirrorCameraPos, 1)).x < 0;
        #endif
    }

    bool is_desktop()
    {
        #ifdef USING_STEREO_MATRICES
        return false;
        #else
        return _VRChatMirrorMode != 1;
        #endif
    }

    bool has_sound_texture() { return _SoundTex_TexelSize.z > 16; }

    struct vertdata
    {
        float4 vertex : POSITION;
        float2 uv : TEXCOORD0;
        UNITY_VERTEX_INPUT_INSTANCE_ID
    };

    // Returns pixel sharpened to nearest pixel boundary. 
    // texelSize is Unity _Texture_TexelSize; zw is w/h, xy is 1/wh
    float2 sharpSample( float4 texSize , float2 coord )
    {
        float2 boxSize = clamp(fwidth(coord) * texSize.zw, 1e-5, 1);
        coord = coord * texSize.zw - 0.5 * boxSize;
        float2 txOffset = smoothstep(1 - boxSize, 1, frac(coord));
        return(floor(coord) + 0.5 + txOffset) * texSize.xy; 
    }

    float lerpstep( float a, float b, float t)
    {
        return saturate( ( t - a ) / ( b - a ) );
    }

    float sharpenLighting (float inLight, float softness)
    {
        float2 lightStep = 0.5 + float2(-1, 1) * fwidth(inLight);
        lightStep = lerp(float2(0.0, 1.0), lightStep, 1-softness);
        inLight = lerpstep(lightStep.x, lightStep.y, inLight);
        return inLight;
    }

    float simpleSharpen (float x, float width, float mid)
    {
        width = max(width, fwidth(x));
        x = lerpstep(mid-width, mid, x);

        return x;
    }

    fixed4 tex2Dmultisample(sampler2D tex, float2 uv)
    {
    float2 dx = ddx(uv) * 0.25;
    float2 dy = ddy(uv) * 0.25;

    float4 sample0 = tex2D(tex, uv + dx + dy);
    float4 sample1 = tex2D(tex, uv + dx - dy);
    float4 sample2 = tex2D(tex, uv - dx + dy);
    float4 sample3 = tex2D(tex, uv - dx - dy);
        
    return (sample0 + sample1 + sample2 + sample3) * 0.25;
    }

    fixed4 tex2DmultisampleSharp(sampler2D tex, float4 ts, float2 uv)
    {
    float2 dx = ddx(uv) * 0.25;
    float2 dy = ddy(uv) * 0.25;

    float4 sample0 = tex2D(tex, sharpSample(ts, uv + dx + dy));
    float4 sample1 = tex2D(tex, sharpSample(ts, uv + dx - dy));
    float4 sample2 = tex2D(tex, sharpSample(ts, uv - dx + dy));
    float4 sample3 = tex2D(tex, sharpSample(ts, uv - dx - dy));
        
    return (sample0 + sample1 + sample2 + sample3) * 0.25;
    }

    // AI spaghetti? code to make inputs a bit more clear

    float2 getVideoDimensions()
    {
        int video_width;
        int video_height;
    #ifndef SHADER_TARGET_SURFACE_ANALYSIS
        _VideoTex.GetDimensions(video_width, video_height);
        return float2(video_width, video_height);
    #else
        return float2(0, 0);
    #endif
    }

    bool isNoVideo(float2 video_dims)
    {
        return video_dims.x <= 16;
    }

    float get3D(bool no_video)
    {
        return no_video ? _3D : abs(_VideoData._41);
    }

    float getWide(bool no_video)
    {
        return no_video ? _Wide - 1 : sign(_VideoData._41);
    }

    bool isSwap(float _3d)
    {
        return _3d == 2 || _3d == 3;
    }

    float getState()
    {
        return _VideoData._12;
    }

    float getSeek()
    {
        return _VideoData._22;
    }

    float getSpread(bool no_video)
    {
        return no_video ? _Spread : _VideoData._42;
    }

    bool isForce2D(bool no_video)
    {
        return no_video ? _Force2D : int(_VideoData._11) >> 4 & 1;
    }

    bool isRightEye(bool swap)
    {
        return swap != is_right_eye();
    }

    float getStereoEyeRight(bool force2d, bool right_eye)
    {
        return !force2d && right_eye ? 0.5 : 0;
    }

    float4 getUVClip()
    {
        return float4(0, 0, 1, 1);
    }

    bool isOnlySound(bool no_video, float state, float seek)
    {
        return no_video && has_sound_texture() && state > 1 && seek > 0 && seek < 1;
    }

    float2 getUV(float2 uv, bool only_sound, bool no_video)
    {
        if (only_sound) uv = uv * _SoundTex_ST.xy + _SoundTex_ST.zw;
        else if (no_video) uv = uv * _MainTex_ST.xy + _MainTex_ST.zw;
        else uv = uv * _VideoTex_ST.xy + _VideoTex_ST.zw;
        return uv;
    }

    float4 getTexelSize(float2 videoDims, bool only_sound, bool no_video)
    {
        if (only_sound) return _SoundTex_TexelSize;
        else if (no_video) return _MainTex_TexelSize;
    #ifndef SHADER_TARGET_SURFACE_ANALYSIS
        else return float4(1.0 / videoDims, videoDims);
    #endif
        return 0;
    }

    float2 getUVCenter(float4 uv_clip)
    {
        return (uv_clip.xy + uv_clip.zw) * 0.5;
    }

    bool isOutsideClipZone(float2 uv, float4 uv_clip)
    {
        return any(uv <= uv_clip.xy) || any(uv >= uv_clip.zw);
    }

    float4 sampleTexture(bool only_sound, bool no_video, float2 uv)
    {
        if (only_sound) return tex2D(_SoundTex, uv);
        else if (no_video) return tex2D(_MainTex, uv);
    #ifndef SHADER_TARGET_SURFACE_ANALYSIS
        else return _VideoTex.Sample(sampler_VideoTex, uv);
    #endif
        return 0;
    }

    struct VideoData
    {
        float2 videoDims;
        bool noVideo;
        float is3D;
        float isWide;
        bool swap;
        float state;
        float seek;
        float spread;
        bool force2d;
        bool rightEye;
        float stereoEyeRight;
        float4 uvClip;
        bool onlySound;
        float2 uv;
        float4 texelSize;
        float2 uvCenter;
    };

    VideoData getVideoData(const float2 texcoords)
    {
        VideoData data;
        data.videoDims = getVideoDimensions();
        data.noVideo = isNoVideo(data.videoDims);
        data.is3D = get3D(data.noVideo);
        data.isWide = getWide(data.noVideo);
        data.swap = isSwap(data.is3D);
        data.state = getState();
        data.seek = getSeek();
        data.spread = getSpread(data.noVideo);
        data.force2d = isForce2D(data.noVideo);
        data.rightEye = isRightEye(data.swap);
        data.stereoEyeRight = getStereoEyeRight(data.force2d, data.rightEye);
        data.uvClip = getUVClip();
        data.onlySound = isOnlySound(data.noVideo, data.state, data.seek);
        data.uv = getUV(texcoords, data.onlySound, data.noVideo);
        data.texelSize = getTexelSize(data.videoDims, data.onlySound, data.noVideo);
        data.uvCenter = getUVCenter(data.uvClip);
        return data;
    }

    VideoData adjustUVForMirror(VideoData data)
    {
        if (_Mirror == 1) data.uv.x = lerp(data.uv.x, 1 - data.uv.x, is_mirror());
        else if (_Mirror == 2)
        {
            data.uv.x = lerp(data.uv.x, 1 - data.uv.x, ddx(data.uv.x) < 0);
            data.uv.y = lerp(data.uv.y, 1 - data.uv.y, ddy(data.uv.y) < 0);
        }
        return data;
    }

    VideoData adjustUVFor3D(VideoData data)
    {
        if (data.is3D == 1 || data.is3D == 2) // side-by-side
        {
            // clip adjustment
            data.uvClip.x = data.stereoEyeRight;
            data.uvClip.z = data.stereoEyeRight + 0.5;
            // eye correction
            data.uv.x = data.uv.x * 0.5 + data.stereoEyeRight;
            // correct for SBS-Full mode
            data.videoDims.x = lerp(data.videoDims.x, data.videoDims.x / 2, data.isWide < 0);
            // split eye offset (VR Only), for 3D where the images do not match up normally
            if (!is_desktop() && !data.force2d)
                data.uv.x = data.uv.x + lerp(data.spread * (0.5 - data.uv.x), -data.spread * (data.uv.x - 0.5), data.rightEye);
        }
        else if (data.is3D == 3 || data.is3D == 4) // over-under
        {
            // clip adjustment
            data.uvClip.y = data.stereoEyeRight;
            data.uvClip.w = data.stereoEyeRight + 0.5;
            // eye correction
            data.uv.y = data.uv.y * 0.5 + data.stereoEyeRight;
            // correct for OVUN-Full mode
            data.videoDims.y = lerp(data.videoDims.y, data.videoDims.y / 2, data.isWide < 0);
            // split eye offset (VR Only), for 3D where the images do not match up normally
            if (!is_desktop() && !data.force2d)
                data.uv.x = data.uv.x + lerp(data.spread * (1 - data.uv.x), -data.spread * (data.uv.x - 0), data.rightEye);
        }
        return data;
    }

    VideoData adjustUVForAspectRatio(VideoData data)
    {
        if (data.onlySound) data.uv = aspect_ratio(data.uv, _Aspect, _SoundTex_TexelSize.zw, data.uvCenter);
        else if (data.noVideo) data.uv = aspect_ratio(data.uv, _Aspect, _MainTex_TexelSize.zw, data.uvCenter);
        else data.uv = aspect_ratio(data.uv, _Aspect, data.videoDims, data.uvCenter);
        return data;
    }

    void surf (Input IN, inout SurfaceOutputStandard o) {

        VideoData data = getVideoData(IN.uv_dummy);
        data = adjustUVForMirror(data);
        data = adjustUVFor3D(data);
        data = adjustUVForAspectRatio(data);

        
        float2 resScale = 1+floor(float2(_TargetResX, _TargetResY)/data.texelSize.zw);
        float2 apUV = data.uv*data.texelSize.zw*resScale;
        float2 apSharpUV = sharpSample(_Aperture_TexelSize,apUV);
        float3 mask = tex2D (_Aperture, apSharpUV) * 2.0;

        data.uv = sharpSample(data.texelSize, data.uv);
        float4 tex = sampleTexture(data.onlySound, data.noVideo, data.uv);

        #if defined(_INVERSETONEMAP)
        tex.rgb = NeutralTonemapInvert(tex);
        #endif  

        tex *= _Brightness;
        if (isOutsideClipZone(data.uv, data.uvClip))
        {
            if (_Clip) discard;
            tex = float4(0, 0, 0, 1);
        }

        float emissionPower = _Emission;

        #if defined(SHADER_API_MOBILE)
        // On Android, which is non-HDR, clamp the output.
        emissionPower = saturate(emissionPower);
        #endif

/*
        if (_IsAVProInput == 1)
        {
                e.rgb = pow(e.rgb,2.2);
        }
*/

        #if !UNITY_PASS_META
        o.Emission = tex * emissionPower * mask;
        #else
        o.Emission = tex * emissionPower * _EmissionMeta;
        #endif
        o.Metallic = 0;
        o.Smoothness = _Smoothness;
    }
    ENDCG
  }
  FallBack "Video/RealtimeEmissiveGamma"
}
